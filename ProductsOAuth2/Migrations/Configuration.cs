namespace ProductsOAuth2.Migrations
{
    using ProductsOAuth2.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProductsOAuth2.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProductsOAuth2.Models.ApplicationDbContext context)
        {
            context.Products.AddOrUpdate(x => x.Id,
               new Product() { Id = 1, Name = "Product1", Price = 100, Date = "01.11.2018" },
               new Product() { Id = 2, Name = "Product2", Price = 200, Date = "01.11.2018" },
               new Product() { Id = 3, Name = "Product3", Price = 300, Date = "01.11.2018" }
               );
        }
    }
}
