﻿using ProductsOAuth2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsOAuth2.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
        Product GetById(int id);
        void Create(Product product);
        void Delete(Product product);
        void Update(Product product);
    }
}