﻿using ProductsOAuth2.Interfaces;
using ProductsOAuth2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProductsOAuth2.Controllers
{
    [Authorize]
    public class ProductsController : ApiController
    {

        public IProductRepository  repository { get; set; }

        public ProductsController(IProductRepository _repository)
        {
            repository = _repository;
        }

        [AllowAnonymous]
        public IHttpActionResult GetProducts()
        {
            return Ok(repository.GetAll());
        }

        
        public IHttpActionResult GetProduct(int id)
        {
            var product = repository.GetById(id);
            if (product==null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        public IHttpActionResult PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            repository.Create(product);
            return CreatedAtRoute("DefaultApi",new { product.Id }, product);
        }


        public IHttpActionResult DeleteProduct(int id)
        {
            var product = repository.GetById(id);
            if (product == null)
            {
                return NotFound();
            }

            repository.Delete(product);
            return Ok();
        }       

        public IHttpActionResult PutProduct(int id,Product product)
        {
            if (id != product.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                repository.Update(product);
            }
            catch 
            {

                return BadRequest();
            }

            return Ok(product);
        }
    }
}
