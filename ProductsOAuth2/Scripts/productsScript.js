﻿$(document).ready(function () {

    var host = window.location.host;
    var token = null;
    var headers = {};

    $("#nav-logOut").css("display", "none");

    $("#addProduct").css("display", "none");

    $("body").on("click", "#deleteBtn", deleteProduct); 
    $("body").on("click", "#editBtn", editProduct);


    $(document).ready(function () {

        $.ajax({

            "url": "http://" + host + "/api/products",
            "type": "GET"

        }).done(showProducts);

    });

            // REGISTER 
    $("#register").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

       
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };


        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#info").append("Registration was successful").fadeOut(5000);
            console.log(data);
        }).fail(function (data) {
            alert(data);
        });


    });

       // LOG IN

    $("#logIn").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#user-info").empty().text(" user: " + data.userName);
            token = data.access_token;
            $("#logIn").css("display", "none");
           // $("#deleteBtn").css("display", "block");
           // $("#editBtn").css("display", "block");
            $("#register").css("display", "none");
            $("#nav-logOut").css("display", "block");
            $("#addProduct").css("display", "block");

        }).fail(function (data) {
            alert(data);
        });
    });

        // LOG OUT
    $("#nav-logOut").click(function () {
        token = null;
        headers = {};

        $("#logIn").css("display", "block");
        $("#register").css("display", "block");
        $("#nav-logOut").css("display", "none");
       // $("#deleteBtn").css("display", "none");
       // $("#editBtn").css("display", "none");
        $("#info").empty();
        

    });

    // DISPLAY PRODUCTS
    function showProducts(data) {

        console.log(data);

        container = $("#products");
             
        var table = $("<table></table>").addClass("table table-bordered table-striped table-hover");
        var header = $("<thead><tr><th>Id</th><th>Name</th><th>Price</th><th>Date</th></tr></thead>").addClass("thead-dark");
        table.append(header);
        var tbody = $("<tbody></tbody>");
        var i;
        for (i = 0; i < data.length; i++) {
            
            var row = $("<tr><td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].Price + "</td><td>" + data[i].Date + "</td></tr>");
            var editBtn = $("<td><button name='"+ data[i].Id +"' id='editBtn' type='button' class='btn btn-sm btn-info'>Edit</button></td>");
            var deleteBtn = $("<td><button name='"+ data[i].Id +"' id='deleteBtn' type='button' class=' btn btn-sm btn-danger'>Delete</button></td>");
            row.append(editBtn, deleteBtn);
            tbody.append(row);
        }

        table.append(tbody);
        container.append(table);
    }

       //POST product 

    $("#prodForm").submit(function (e) {
        e.preventDefault();

        var name = $("#prodName").val();
        var price = $("#prodPrice").val();
        var date = $("#prodDate").val();

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var prod = {
   
            "Name": name,
            "Price": price,
            "Date": date
        };

        $.ajax({
            "type": "POST",
            "url": "http://" + host + "/api/products/",
            "headers": headers,
            "data": prod

        }).done(function (data) {
            alert("Product added");
            console.log(data);
           
        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    });

        //DELETE PRODUCT
    function deleteProduct() {

        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var deleteID = this.name;

        $.ajax({
            "type": "DELETE",
            "url": "http://" + host + "/api/products/" + deleteID,
            "headers": headers

        }).done(function (data) {
            alert("Deleted!");

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });
    }


    function editProduct() {
    
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        var editID = this.name;

        var prod = {
            "Id": editID,
            "Name": name,
            "Price": price,
            "Date": date
        };

       

        $.ajax({
            "type": "PUT",
            "url": "http://" + host + "/api/products/" + editID,
            "headers": headers,
            "data":prod


        }).done(function (data) {
            alert("update!");

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });

    }

});